/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.chanisata.lab2_ox_game;

import java.util.Scanner;

/**
 *
 * @author ASUS
 */
public class Lab2_OX_Game {

    public static char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    private static char currentPlayer = 'X';
    static int row = 0;
    static int col = 0;
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        boolean playAgain = true;
        printWelcome();
        while (playAgain) {
            resetTable();
            while (true) {
                printTable();
                printTurn();
                inputRowNCol();

                if (isWinner()) {
                    printTable();
                    printWinner();
                    break;
                }

                if (isDraw()) {
                    printTable();
                    printDraw();
                    break;
                }
                switchPlayer();
            }
            playAgain = playAgain();
        }
    }

    private static void printWelcome() {
        System.out.println("- Welcome to OX games -");
        System.out.println("Start!");
    }

    public static void printTable() {
        System.out.println("-----------------");
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print("|" + table[i][j] + "|");
            }
            System.out.println("\n-----------------");
        }
    }

    private static void printTurn() {
        System.out.println("> " + currentPlayer + " Turn!! <");
    }

    private static void inputRowNCol() {
        System.out.print(currentPlayer + " please input row & col : ");
        row = sc.nextInt();
        col = sc.nextInt();

        if (table[row - 1][col - 1] == '-') {
            table[row - 1][col - 1] = currentPlayer;
        }
    }

    private static void switchPlayer() {
        if (currentPlayer == 'O') {
            currentPlayer = 'X';
        } else {
            currentPlayer = 'O';
        }
    }

    private static boolean isWinner() {
        if (checkRow() || checkCol() || checkX1() || checkX2()) {
            return true;
        }
        return false;
    }

    private static boolean checkRow() {
        for (int i = 0; i < 3; i++) {
            if (table[row - 1][i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkCol() {
        for (int i = 0; i < 3; i++) {
            if (table[i][col - 1] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkX1() {
        if (table[0][0] == currentPlayer && table[1][1] == currentPlayer && table[2][2] == currentPlayer) {
            return true;
        }
        return false;
    }

    private static boolean checkX2() {
        if (table[0][2] == currentPlayer && table[1][1] == currentPlayer && table[2][0] == currentPlayer) {
            return true;
        }
        return false;
    }

    private static void printWinner() {
        System.out.println(currentPlayer + " is a winner! ");
    }

    private static boolean isDraw() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (table[i][j] == '-') {
                    return false;
                }
            }
        }
        return true;
    }

    private static void printDraw() {
        System.out.println("It's a draw!");
    }

    private static void resetTable() {
        table = new char[][]{{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    }

    private static boolean playAgain() {
        String choice = sc.nextLine().toUpperCase();
        while (!choice.equals("y") && !choice.equals("n")) {
            System.out.print("Do you wanna play again? (y/n) : ");
            choice = sc.nextLine().toUpperCase();
        }
        return choice.equals("y");
    }

}
